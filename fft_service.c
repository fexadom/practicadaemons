#include "csapp.h"
#include "fft.h"
#include "commands.h"

const size_t S_DATA = sizeof(complex)*N;

enum Estado {EXIT = 0, WAITING_COMMAND, WAITING_DATA, DOING_FFT, SENDING_DATA, DISCONNECTING} estado; //Estados para la FSM

void responder_cliente(int connfd);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	estado = DISCONNECTING;
	listenfd = Open_listenfd(port);
	while (estado) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);

		responder_cliente(connfd);
		printf("desconectando al cliente ...\n");
		Close(connfd);
	}
	printf("Bye...\n");
	exit(0);
}

void responder_cliente(int connfd)
{
	char buf[MAXLINE];
	complex data[N];
	complex data_tmp[N];
	size_t n;
	rio_t rio;

	estado = WAITING_COMMAND;
	Rio_readinitb(&rio, connfd);
	while(estado) {
		switch(estado)
		{
			case WAITING_COMMAND:
				n = Rio_readlineb(&rio, buf, MAXLINE);

				if(n>0)
				{
					if(strcmp(buf,STOP) == 0){
						Rio_writen(connfd, QUIT, 5);
						estado = EXIT;
					} else if (strcmp(buf,DISCONNECT) == 0){
						estado = DISCONNECTING;
					} else if (strcmp(buf,FFT) == 0){
						estado = WAITING_DATA;
					} else {
						Rio_writen(connfd, UNKNOWN, 3);
						printf("Comando desconocido\n");
					}
				}else
					estado = DISCONNECTING;
				break;
			case WAITING_DATA:
				Rio_writen(connfd, OK, 3);
				printf("Recibiendo trama de %d puntos\n",N);
				size_t n = Rio_readn(connfd, (void *)data, S_DATA);
				if(n == S_DATA)
					estado = DOING_FFT;
				else {
					printf("Error recibiendo datos, desconectando...");
					estado = DISCONNECTING;
				}
				break;
			case DOING_FFT:
				fft(data,N,data_tmp);
				estado = SENDING_DATA;
				break;
			case SENDING_DATA:
				Rio_writen(connfd, (void *) data, S_DATA);
				estado = WAITING_COMMAND;
				break;
			case DISCONNECTING:
				Rio_writen(connfd, BYE, 4);
				return;
				break;
			default:
				estado = DISCONNECTING;
		}
	}
}
